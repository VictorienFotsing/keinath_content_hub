<?php 
		get_header();
		global $post;
?>

	<section  >
		<div class="termin_img header_banner" style="background-image:url('/wp-content/uploads/2018/12/termine-und-service-keinath-stage.jpg');">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h2 class="Terminbanner-head">Schneller zum Führerschein: </h2>
					<h2 class="Terminbanner-head">mit unseren Intensiv-Kursen</h2>
				</div>
				<div class="col-lg-6">
				</div>
			</div>
		</div>		
	</section>

	<section>
		<?php
			$args = array(
				'post_type' => 'termin',
				'numberposts' => -1,
				'order' => 'ASC'
			  );
		  
			  $termine = get_posts($args);
			  echo "<div class='container'>";
			  echo "<div><h2 class='termin_title termin-mr'>Jetzt Kontakt aufnehmen</h2><div>";
			  echo "<div class='row'>";
					  echo "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3'>";
					  	  $jahresplanerPagesArgs = get_pages(
							array(
								'child_of' => '2151',
								'sort_order' => 'ASC',
								'sort_column' => 'menu_order',
								'posts_per_page' => '-1'
							)
						  );
							echo '<div class="theo_collection">';
							echo '<a class="planer_theo_link" href="'.get_permalink(2151).'" title="Jahresplaner Theo">Jahresplaner Theo</a>';
							echo "<ul>";
							foreach ($jahresplanerPagesArgs as $singlePage){
								echo '<li><a class="planer_theo_link" href="'.get_permalink($singlePage->ID).'" title="'.$singlePage->post_title.'">'.$singlePage->post_title.'</a></li>';
							}
							echo "</ul>";
							echo '</div>';
						foreach ($termine as $singleTermin){
							// List categories --> Link to Category Archive
							$keyCollection[] = get_category(wp_get_post_categories($singleTermin->ID)[0])->name;
							//print_r(get_category(wp_get_post_categories($singleTermin->ID)));
						}
						$keyCollection = array_unique($keyCollection);
					
						foreach($keyCollection as $key => $value){
							$str_val = str_replace('–','',$value);
							echo "<div class='hashtag'><a class='termin_val' href='/termine-service/#terminCat=".str_replace(' ','',$str_val)."'>".$value. "</a><br></div>"; // with Hash Tag
						}
						
					echo '<br><br><img src="/wp-content/themes/keinath/assets/images/fun_learn.png" width="150">';
					echo "</div>";
					echo "<div class='col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9 termin-inputfields'>";
						echo "<div class ='termins'>";
						foreach($termine as $singleTermin){
							$key_val = get_category(wp_get_post_categories($singleTermin->ID)[0])->name; // Data-for and Hash Tag
							$hashTag_key = str_replace('–','', str_replace(' ','',$key_val)); // Hash Tag
							//echo $hashTag_key;
							echo "<div id ='$hashTag_key' class='termins_data' data-for='$key_val'>";
							echo "<div class='termintitle'><a href='".get_permalink($singleTermin->ID)."'>".get_field('title', $singleTermin->ID)."</a></div>";
									echo "<div class='row start-endDate'>
									<div class='col-lg-12'>".get_field('start-date', $singleTermin->ID)."</div>
									</div>";
									echo "<div class='row start-endDate'>
											<div class='col-lg-4'>".get_field('location', $singleTermin->ID)."</div>
											<div class='col-lg-4'><span class='link'><a href='".get_permalink($singleTermin->ID)."'>Jetzt anmelden</a></span></div>
									</div>";
							echo "</div>";	
						}
						echo "</div>";
						echo "<div class='form'>";		  
								echo "<input type='hidden' class='kurstitle' name='kurstitle' value='".get_field("title", $post->ID)."'>";
								echo "<input type='hidden' class='kursdate' name='kursdate' value='".get_field("start-date", $post->ID)."'>";	
								//echo get_field("starting-date", $post->ID);
								echo do_shortcode( '[contact-form-7 id="1402" title="Termin-form"]' );
						echo "</div>";	  
						
					echo "</div>";
			echo "</div>";		
		echo "</div>";
		?>
		
	</section>

<?php 
		get_footer();
?>