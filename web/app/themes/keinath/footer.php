<!-- Footer -->
    <footer class="py-5">
      <div class="container">
		  <div class="row">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
						<address>Fahrschule Keinath GbR<br>
							Augsburger Str. 40, 86157 Augsburg</address>
						<div>
						<span>Büro: 08 21/52 49 00</span><br>
						<span>Mo–Fr: 09.00–18.30 Uhr</span><br>
						<span>service@fahrschule-keinath.de</span><br>
						</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
						<div class="footer_menu clearfix">
						<?php wp_nav_menu(array(
						'theme_location'=>'footer',// name of menu
						'menu_class'=>'navbar-nav-footer ml-auto',// change ul class
						'menu_id'=>'',//change ul id
						'container'=>'',// add div wrap
						'container_class'=>'',
						'container_id' =>'', // like above
						'link_after' =>'',
						'link_after' =>'<span>|' 
						));?>
						</div>
						<div class="row footer_img">
							<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pr-sm-1 pr-0"><img src="<?php bloginfo('template_directory'); ?>/assets/images/footer-logo1.png"></div>
							<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 pr-sm-1 pr-0"><img src="<?php bloginfo('template_directory'); ?>/assets/images/footer_logo2.png" height="68"></div>
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pl-lg-1 pl-md-5 pl-sm-4 pl-0 footer_sidetext"><p>Mitglied im Verbund "Erfolgreiche Fahrschulen"</p><p><a href="https://www.erfolgreiche-fahrschule.de/" target="_blank" style="color: #fff;text-decoration: none;" rel="nofollow">www.erfolgreiche-fahrschule.de</a></p></div>
						</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
					<div>
					<p>
					“ Seit über 80 Jahren begleiten wir Fahrschüler auf ihrem Weg. ”
						<a href="/#kundenmeinungen" class="underline_text">Zu den Kundenmeinungen</a>
					</p>
					</div>
                  	<div class="social_icons">
						<div class="single_icon"><a href="https://www.facebook.com/pages/Keinath-Gruppe/287818861341779?sk=timeline" target="_blank"><i class="fab fa-facebook-f c_icon"></i></a></div>
						<div class="single_icon"><a href="https://twitter.com/KeinathGruppe" target="_blank"><i class="fab fa-twitter c_icon"></i></a></div>
						<div class="single_icon"><a href="https://www.youtube.com/channel/UCU6vRyGzSoEtefv9OOeF8PA" target="_blank"><i class="fab fa-youtube c_icon"></i></a></div>
						<div class="single_icon"><a href="https://instagram.com/fahrschulekeinath/" target="_blank"><i class="fab fa-instagram c_icon"></i></a></div>
				  </div>
				</div>
			</div>	
		</div>
		  
      <!-- /.container -->
    </footer>
	<?php 
	
	if (is_front_page()){
		?>
	<div class="stageBottom">
		<div class="container">
			<div class="car_icon"><img src="/wp-content/uploads/2018/10/car.png"></div>
			<span>Freiheit erfahren – mit der Fahrschule Keinath</span>
			<div class="liveChatTeaser"><a href="/kontakt/">Zum Kontaktformular</a></div>
		</div>
	</div>
	<?php }
	?>
    <!-- Bootstrap core JavaScript -->
    <!-- <script src="<?php bloginfo('template_directory'); ?>/assets/js/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.bundle.min.js"></script> -->
	 <script src="<?php bloginfo('template_directory'); ?>/assets/js/custom.js"></script>
    <?php wp_footer(); ?>
  </body>

</html>
