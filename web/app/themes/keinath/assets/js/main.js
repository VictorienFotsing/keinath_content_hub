document.getElementById('mobile-menu').addEventListener('click', function(){ 
    var x = document.getElementById('mobile-overlay'); 
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }; 
}); 

/* Slick Slider */
$(document).ready(function(){
    // $('.slider').slick({
    //     dots: true,
    //     arrows: true, 
    //     autoplay: false, 
    //     slidesToShow: 3,
    //     infinite: false,
    //     responsive: [
    //         {
    //           breakpoint: 600,
    //           settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1
    //           }
    //         }
    //     ]
    // });
});
var slider = tns({
    container: '.slider',
    controls: false,
    slideBy: "page",
    gutter: 16,
    mouseDrag: true,
    swipeAngle: false,
    arrowKeys: true,
    responsive: {
        700: {
            items: 1
        },
        800: {
            items: 2
        },
        1200: {
            items: 3
        }
    }
  });