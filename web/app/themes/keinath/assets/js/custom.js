$(document).ready(function(){
/*
  var siteUrl = window.location.href ;

  // polyfill ES6
  if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
      'use strict';
      if (typeof start !== 'number') {
        start = 0;
      }
      
      if (start + search.length > this.length) {
        return false;
      } else {
        return this.indexOf(search, start) !== -1;
      }
    };
  }

  if (siteUrl.includes('termine-service/#ErsteHilfe')) {
      setTimeout(function() {
        $('.single-termin').hide();
        $('.termins div[data-for = "Erste Hilfe"]').show();
    }, 10); 
  }

  if (siteUrl.includes('termine-service/#ferienkurse')) {
    setTimeout(function() {
        $('.single-termin').hide();
        $('.termins div[data-for = "Ferienkurse"]').show();
    }, 10);
  }

  if (siteUrl.includes('termine-service/#turbotheo')) {
    setTimeout(function() {
        $('.single-termin').hide();
        $('.termins div[data-for = "TT – Turbo Theo"]').show();
    }, 10);
  }
*/
 
 
   $('.navbar-nav li').click(function(){
       $('.navbar-nav li a').removeClass('active_menu');
       $('.navbar-nav li a').addClass('active_menu');
   });
   /* menu active end */

   /* anzeigen start */ 
   $('.up-angle').hide();
   $('.ads_show').click(function(){
       //$( '.ad_box:not(.dontCollapse)' ).slideToggle( 'slow' );
       $( '.ad_box' ).slideToggle( 'slow' );
       $( '.ads_divider' ).slideToggle( 'slow' );
       $('.ads_show').find('.up-angle, .down-angle').toggleClass('up-angle down-angle');
   });
   /* anzeigen End */ 

   /*Auto anzeigen start */ 
   $('.up-angle').hide();
   $('.auto_show').click(function(event){
       event.preventDefault();
       $( '.mbl_ad' ).slideToggle( 'slow' );
       $('.auto_show').find('.up-angle, .down-angle').toggleClass('up-angle down-angle');
       
   });
   /*Auto anzeigen End */

   /* überkeinath anzeigen start */ 

   $('.lg-up-angle').hide();
   checkSlidingDivBoxes();

   $(window).on('resize', function(){
    checkSlidingDivBoxes();
    });

   function checkSlidingDivBoxes(){
        var thisIndex = -1;
        $('section.sliding-div').each(function() {
            //var index = $(this).index();
            //console.log(index);
            var showAll = $(this).find('.lg-up-angle').is(':visible');
             if (thisIndex == (index - 1)) {
                 return false;
             } else {
                var index = $(this).index();
                 thisIndex = index;
                 //console.log(thisIndex);
             }
            //var $firstRow = $('section:nth-child(' + (thisIndex + 1) + ') section:nth-child(2)'); 
            var $firstRow = $('section:nth-child(' + (thisIndex + 1) + ') section:first-of-type');
            var $first = $firstRow.find('.elementor-row > div:nth-child(1)');
            var $second = $firstRow.find('.elementor-row > div:nth-child(2)');
            var $third = $firstRow.find('.elementor-row > div:nth-child(3)');

            $(this).find('section .elementor-row > div').each(function() {
                if (!showAll) {
                    $(this).addClass('detail-box-hidden');
                } else {
                    $(this).removeClass('detail-box-hidden');
                }
            });

            $first.removeClass('detail-box-hidden');
            if ($(window).width() >= 768) {
                $second.removeClass('detail-box-hidden');
                $third.removeClass('detail-box-hidden');
            }

        });
   }
    
    $('.person_Adshow').click(function(event){
        event.preventDefault();
        var $parent = $(this).closest('.sliding-div');
        $parent.find('.lg-down-angle').toggle();
        $parent.find('.lg-up-angle').toggle();
        checkSlidingDivBoxes();
        // return falso so no execution or redirection
        return false;
    });

   /* überkeinath anzeigen End */

   /* weiter lesen start */ 
   $('.content_show').addClass('hide');
   $('.hide_detail').addClass('hide');
   $('.more_detail').click( function(){
       var $parent = $(this).closest('.elementor-container');
       $parent.find('.content_show').show('slow');
       $parent.find('.hide_detail').show('slow');
       $parent.find('.more_detail').hide('slow');
       $parent.find('.continue').hide('slow');
   });
   $('.hide_detail').click( function(){
       var $parent = $(this).closest('.elementor-container');
       $parent.find('.content_show').hide('slow');
       $parent.find('.hide_detail').hide('slow');
       $parent.find('.more_detail').show('slow');
       $parent.find('.continue').show('slow');
   });
   /* weiter lesen End */ 
   
   /* Termine & Service Seite Termine Module */
      $('.single-termin').show();
      $('.termin_menu').click(function(){
        $('.single-termin').hide();  
        var $data = $(this).data('for');
        //alert ($data);
        $('.termins div[data-for = "'+$data+'"]').show();
      });
      //$('.termin_menu:first-child').trigger('click');
   /* End */
  
    /* "Jetzt Kontakt aufnehmen" further page of TERMINE AND SERVICES*/
    $('.termins_data').hide();
    $('.termin_val').click(function(e){

        /*  jquery with data-for */
        /*var val = $(this).data('for');
        //alert(val);
        $('.form').hide();
        $('.termins_data').hide();
        $('div[data-for = "'+ val +'"].termins_data').show();*/
        /*  jquery with data-for [END] */
        
        /*  jquery with Hash Tag */
        if(this.hash !== '' ){
            e.preventDefault();
        }
        var hash_val=this.hash;
        var hash_retrive =hash_val.substring(11);
        //var divID_Value = $('#'+ hash_retrive).attr('id');
        $('.form').hide();
        $('.termins_data').hide();
        $('div[id ^= "'+ hash_retrive +'"]').show();
       
        /*  jquery with Hash Tag [END] */
        
    });
    /* End */       

   /* Termine Module FORM input field data*/
        $('input.kurs').val($('input[name = "kurstitle"]').val());
        $('input.kurs').attr('readonly',true); // add read only atribute to input field
        $("input.Kursdatum").val($("input[name='kursdate']").val());
        $("input.Kursdatum").attr('readonly',true); // add read only atribute to input field

      /* This is also working*/  
   /* End */
   
   /* link pages with jquery*/
   $('.openGoeggingen').click(function(){
      $('#goggingen').trigger('click');
   });
   $('.openPfersee').click(function(){
      $('#pfersee').trigger('click');
   });

     
    
   /* END */
   /* Google map Change on Click */
   $('.address_show div').hide();
   $('#google_map li a').click(function(e){
       e.preventDefault();
       var value= $(this).attr('id');
       //alert(value);
       //$('.fahrschule_map').addClass(value);
       $('.fahrschule_map').attr('id',value);
       
       $('iframe').addClass(value);
       $('.address_show >div').hide(); 
       if($('iframe').attr('class')){
           //alert($('iframe').attr('class'));
           var addresses = {
            maximilianstrasse: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10603.279921193762!2d10.909610875923597!3d48.363982725573486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479ebd5526980873%3A0xff4727bd90711d9f!2sMPU+Beratung+und+Vorbereitung+-+Keinath!5e0!3m2!1sde!2sde!4v1542990490588',
            pfersee: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2650.901021610232!2d10.866309315951007!3d48.362425543452105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479ea2cc42839f31%3A0x50c52448f743c580!2sFahrschule+Keinath!5e0!3m2!1sde!2sde!4v1542990396766',
            goggingen:'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10608.276168712671!2d10.864688182620265!3d48.33997823195911!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8f1ba4ada5c2d410!2sFahrschule+Keinath+und+MPU+Zweigstelle!5e0!3m2!1sde!2sde!4v1542990344787',
            univiertel: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d557.594583476261!2d10.903625627030143!3d48.33396655671366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479e987031e6ba4f%3A0xbc5c6c86015ee243!2sHermann-K%C3%B6hl-Stra%C3%9Fe+15%2C+86159+Augsburg!5e0!3m2!1sde!2sde!4v1556625571620!5m2!1sde!2sde',
            leitershofen: 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2651.5802172034832!2d10.844861315950727!3d48.34937354436433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479ea2dfa2aabc13%3A0xfb6e74b2f0bac40!2sKeinath+Gruppe!5e0!3m2!1sde!2sde!4v1542990251595'
           };


            //$('iframe').attr('src', addresses[value]); // working code
            $('iframe[class ^="'+value+'"]').attr('src',addresses[value]); // iframe show working code

            
       }
       $('iframe').removeClass(value); 

       $('.address_show >div[id ^= "'+ value +'"]').show(); // address div show
    });
   /* Google map Change on Click [END] */

   /* Job module */

    jobsliding();
    
    function jobsliding(){
       var downangle = $('.jobtoggel').find ($('.job-downangle')).is(':visible');
       //alert(downangle);
        if(!downangle){
            //$('.job_form').removeClass('job-box-hidden');
            $('.job_profile_des').removeClass('job-box-hidden');
        }
        else{
            //$('.job_form').addClass('job-box-hidden');
            $('.job_profile_des').addClass('job-box-hidden');
        }
       
    }
    $('.job-upangle').hide();
    $('.jobtoggel').click(function(){
        $('.job-downangle').toggle();
        $('.job-upangle').toggle();
        jobsliding();
        // return falso so no execution or redirection
        return false;
    });
   /* Job module End */


   /* Elementor widget */
      $('.data-click').css({'overflow': 'hidden', 'height': '0'});
      $('.h_tabs').click(function(){
          /* Elementor widget active button */
           $('.h_tabs').removeClass('h_tabsactive');
           $(this).addClass('h_tabsactive');
           /*End  Elementor widget active button */
           $('.data-click').css({'overflow': 'hidden', 'height': '0'});
           var element = $(this).data('for');
           $('.tabs-contents div[data-for ="'+element+'"]').css({'overflow' : 'auto', 'height' : 'auto '});
      });
      $('.h_tabs:first-child').trigger('click');
   /* Elementor widget End*/

   /* Slick slider for Elementor widget */
   //var $jq = jQuery.noConflict();
   if ($(".lazy").length){
       $(".lazy").slick({
           lazyLoad: 'ondemand', // ondemand progressive anticipated
           infinite: true,
           arrows : true,
           autoplay: true,
           autoplaySpeed: 2000,
           responsive: [
               {
               breakpoint: 900,
               settings: {
                   /*dots: true,*/
                   slidesToScroll: 1,
                   autoplay: true,
                   arrows : false,
               }
               },
           ]
       });
       }
   /* Slick slider for Elementor widget End*/

  /* Mobile Menu*/
    $('.menu-toggle').click(function(){
        if($(window).width() <= 1200){
          $('.custom_nav').toggleClass('open');
        }
        else{
          $('.custom_nav').removeClass('open');

        }
    });
    

    $('.custom_nav ul li').click(function() {

      var currentstate = $('ul.sub-menu', this).hasClass('open');
      
      //alert(this.id);
      $('.custom_nav ul ul.sub-menu').removeClass('open');
      if(!currentstate){
        $('ul.sub-menu', this).addClass('open');
      }
    });
    
  /* Mobile Menu End*/ 
});


var header = $(".fixed-top");

$(window).scroll(function() {    
   var scroll = $(window).scrollTop();
   if (scroll >= 50) {
       header.addClass("scrolled");
       $('.scroll_visible').css({'display':'flex',});
       $('.scroll_hide').hide();
       $('.scroll_Topmenu').css({'margin-left':'35px','margin-top': '20px'});
       $('.chng1').removeClass('col-lg-4 col-4');
       $('.chng1').addClass('col-lg-1 col-4');
       $('.chng2').removeClass('col-lg-8 col-8');
       $('.chng2').addClass('col-lg-11 col-8');
       /* Search input filed hide on Scroll Top  */
       /*$('.search_field').hide();
       $('.search').show();
       $('.search_icon_hide').show();
       $('.search_icon_hide').removeClass('sIcon_r');*/
       /* Search input filed hide on Scroll Top [END]  */

   } else {
       header.removeClass("scrolled");
       $('.scroll_visible').css({'display':'none',});
       $('.scroll_hide').show();
       $('.scroll_Topmenu').css({'margin-left':'0px'});
       $('.chng1').removeClass('col-lg-1 col-4');
       $('.chng1').addClass('col-lg-4 col-4');
       $('.chng2').removeClass('col-lg-11 col-8');
       $('.chng2').addClass('col-lg-8 col-8');

   }
});


/* menu fixed end */ 