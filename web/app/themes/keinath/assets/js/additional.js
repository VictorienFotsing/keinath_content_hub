$(document).ready(function(e){

	$(".sub-menu a").bind("click", function(e){
	
		if ($("#" + $(this).attr("href").split("#")[1]).length){
			
			var obj = $(this);
			
			setTimeout(function(e){
				$('html,body').animate({scrollTop:$("#" + obj.attr("href").split("#")[1]).offset().top - 80}, 500);
			}, 200);
		}
	});

	$("ul.sub-menu a").bind("click",function(e){
		$(".navbar").removeClass("open");
	});
	
	$("input[name='subject']").val($("title").html());
	

	if (location.hash == '#termine'){
		if ($("#termine").length){
		$('html,body').animate({scrollTop:$("#termine").offset().top - 120}, 10);
		}
	}
	
	if (location.hash.split('=')[0] == '#terminCat'){
		
		setTimeout(function(e){
			$('.single-termin').hide();  
		
			$('.termins div[data-for = "'+location.hash.split('=')[1]+'"]').show();
			$('html,body').animate({scrollTop:$(".elementor-element-c34e169").offset().top - 110}, 500);
		}, 200);
	}
	else {
		if (location.href.indexOf("#") != -1 && location.hash.split('=')[0] != '#terminCat') {
			
		
			setTimeout(function(e){
				$('html,body').animate({scrollTop:$(window.location.hash).offset().top - 80}, 500);
			}, 200);
			
		}	
		
		
		if (location.hash.split('#')[1] == 'fuhrpark/'){
			setTimeout(function(e){
				$('html,body').animate({scrollTop:$("#fuhrpark").offset().top - 80}, 500);
				$(".h_tabs").each(function(){
					if ($(this).attr("data-for").toLowerCase() == location.hash.split('#')[2]){
						$('.h_tabs').removeClass('h_tabsactive');
						$(this).addClass('h_tabsactive');
						$('.data-click').css({'overflow': 'hidden', 'height': '0'});
						var element = $(this).data('for');
						$('.tabs-contents div[data-for ="'+element+'"]').css({'overflow' : 'auto', 'height' : 'auto '});
					}
				});
			}, 1500);
		}
		
	
	}
	
	
	/*
    if ($(window).scrollTop() > 100){
        $("header").addClass("fixed-top").addClass("scrolled");

    }
    else {
        $("header").removeClass("fixed-top").removeClass("scrolled");

    }
	*/
});

$(window).scroll(function(){
    var autofahrt = $(window).scrollTop() / 77;
        if ($(window).scrollTop() > 100){
            $('.navbar').addClass('sticky');
            $(".car_icon").css("margin-left", autofahrt + "%");
    } else{
        $('.navbar').removeClass('sticky');
        $(".car_icon").css("margin-left", 0);
        };
});

