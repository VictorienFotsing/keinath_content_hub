<?php 

	/*
		Template Name: Jahresplaner
	*/

?>

<section class="terminPage">

	<div class="stage">
		<div class="container">
			<div class="stageHeadlines">
				<h2 class="banner-head">Schneller zum Führerschein: </h2>
				<h2 class="banner-head">mit unseren Intensiv-Kursen</h2>
			</div>
		</div>
	</div>

<?php

	get_header(); 
	
    $args = array(
      'post_type' => 'termin',
      'numberposts' => -1,
      'order' => 'ASC'
    );

    $termine = get_posts($args);
	
     echo "<div class='container' id='termine'>";
      echo "<div class='row'>";
              echo "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 terminNavigation'>";
			  $jahresplanerPagesArgs = get_pages(
				array(
					'child_of' => '2151',
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order'
				)
			  );
				echo '<div class="theo_collection">';
				echo '<a class="planer_theo_link" href="'.get_permalink($jahresplanerPagesArgs[0]->ID).'" title="Jahresplaner Theo">Jahresplaner Theo</a>';
				echo "<ul>";
				foreach ($jahresplanerPagesArgs as $singlePage){
					echo '<li><a class="planer_theo_link" href="'.get_permalink($singlePage->ID).'#termine" title="'.$singlePage->post_title.'">'.$singlePage->post_title.'</a></li>';
				}
				echo "</ul>";
				echo '</div>';
                foreach ($termine as $singleTermin){
                    // List categories --> Link to Category Archive
                    $key = get_category(wp_get_post_categories($singleTermin->ID)[0])->name;
                    $keyCollection[] = $key;
                }
                $keyCollection = array_unique($keyCollection);

                foreach ($keyCollection as $key){
                  echo "<div class='termin_menu'><a href='/termine-service/#terminCat=".str_replace(' ','',$key)."'>".$key. "</a><br></div>";
                }
				echo '<br><br><img src="/wp-content/themes/keinath/assets/images/fun_learn.png" width="150">';
                echo "</div>";

?>
<div class="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9 terminContent">
	<?php if ($post->ID != 2151){?>
	<h1>Theorieunterricht <?php echo the_title();?></h1>
	<?php }?>
	<?php the_content(); ?>
	</div>
</section>
<?php 	
	get_footer();
?>