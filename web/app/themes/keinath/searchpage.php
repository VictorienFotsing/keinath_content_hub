<?php 
/*
Template Name: Search Page
*/
get_header();
?>

	
    <section style="height:200px;">
		
	</section>
	<section>
		
			 <div class='container'>
                    <div class='row'>
						<?php //get_search_form(); ?>
						<div class="col-lg-12 ">
							<div class="mysearch_fieldContainer">
                            <!-- <div class="search-page-form" id="ss-search-page-form"><?php get_search_form(); ?></div> -->
									<div class="search-page-form">
											<form role="search" method="get" class="search-form mysearch_form" action="<?php echo home_url( '/' ); ?>">
												
												<label>
													<div class="row">
														<div class="col-lg-10 pr-1"><input type="search" class="search-field mysearch_field form-control"
														placeholder="<?php echo esc_attr_x( 'Ihr Suchbegriff …', 'placeholder' ) ?>"
														value="<?php echo get_search_query() ?>" name="s"
														title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" /></div>
														<div class="col-lg-2 px-0"><input type="submit" class="search-submit my_search_submit"
													value="<?php echo esc_attr_x( 'SUCHE', 'submit button' ) ?>" /></div>
													</div>
													
													
												</label>
												
												
										
											</form>
									</div>
							</div>   
                        </div>
					</div>
					<?php //wp_list_pages(); ?>
			</div>

    </section><br><br><br><br>
    

    <!--<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
        <label for="s" class="assistive-text"><?php _e( 'Search', 'keinath' ); ?></label>
        <input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" id="s" placeholder="<?php esc_attr_e( 'Search &hellip;', 'keinath' ); ?>" />
        <input type="submit" class="submit" name="submit" id="searchsubmit" value="<?php esc_attr_e( 'Search', 'keinath' ); ?>" />
	</form>-->
	
	<!-- <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		<label>
			<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
			<input type="search" class="search-field"
				placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
				value="<?php echo get_search_query() ?>" name="s"
				title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
		</label>
		<input type="submit" class="search-submit"
			value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
	</form> -->

<?php 
		get_footer();
?>