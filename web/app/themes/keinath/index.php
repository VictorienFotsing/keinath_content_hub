<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package Keinath Fahrschule
 * @subpackage Keinath Fahrschule
 * @since Keinath Fahrschule
 */
// include ("header.php");
get_header();
?>

<?php if( have_posts()): while (have_posts()) : the_post(); the_content(); endwhile;endif;?>

<?php
//include ("footer.php");
get_footer();
?>