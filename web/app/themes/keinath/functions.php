<?php
$composer_autoload = '/vendor/autoload.php';
$timber = new \Timber\Timber();
\Timber\Timber::$dirname = array( 'templates', 'views' );
Routes::map('content-hub', function($params) {
    /* please note the different order of arguments vs. Timber::load_template */
    Routes::load('content-hub.php', $params, [], 200);
});


register_nav_menu('header', 'header');
add_filter('nav_menu_css_class', 'menu_item_classes', 10, 4);
function menu_item_classes($classes, $item, $args, $depth)
{
    unset($classes);
    $classes[] = 'nav-item';
    return $classes;
}


register_nav_menu('footer', 'footer');
//register_nav_menu('top_menu','top_menu');

// Give class to <a> tag in custom menu
function add_link_atts($atts)
{
    $atts['class'] = "nav-links";
    return $atts;
}
add_filter('nav_menu_link_attributes', 'add_link_atts');


function wpdocs_theme_name_scripts()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('additional', get_template_directory_uri() . '/assets/css/additional.css');
    wp_enqueue_style('boostrap-min', get_template_directory_uri() . '/assets/css/bootstrap.min.css');

    wp_enqueue_script('jquery-min', get_template_directory_uri() . '/assets/js/jquery.min.js');
    wp_enqueue_script('bootstrap-min', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js');
    wp_enqueue_script('additional', get_template_directory_uri() . '/assets/js/additional.js');
}

add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts');

// define the wp_mail_failed callback 
function action_wp_mail_failed($wp_error)
{
    return error_log(print_r($wp_error, true));
}

// add the action 
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);

//Disable Admin Bar for All Users 
show_admin_bar(false);

add_action('widgets_init', 'my_widgets_init');
function my_widgets_init()
{
    /*sidebar*/
    register_sidebar(array(
        'name' => __('ansprechpartner', 'ansprechpartner'),
        'id' => 'ansprechpartner',
        'description' => __('ansprechpartner', 'ansprechpartnerr'),
        'before_widget' => '<div >',
        'after_widget' => '</div>',
        'before_title' => '<div >',
        'after_title' => '<div class=""></div></div>',
    ));
}
function TerminServices()
{
    $args = array(
        'label' => 'Termine',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'termin'),
        'query_var' => true,
        'menu_position'       => 5,
        'taxonomies'          => array('category'),
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',
        )
    );
    register_post_type('termin', $args);
}
add_action('init', 'TerminServices');
/* Features Image */
add_theme_support('post-thumbnails');
//set_post_thumbnail_size( 1200, 9999 );

/*

    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
));
/*
    * Enable support for Post Formats.
    *
    * See: https://codex.wordpress.org/Post_Formats
    */
add_theme_support('post-formats', array(
    'aside',
    'image',
    'video',
    'quote',
    'link',
    'gallery',
    'status',
    'audio',
    'chat',
));


function displayTermine()
{


    $args = array(
        'post_type' => 'termin',
        'numberposts' => -1,
        'orderby' => 'meta_value',
        'order' => 'ASC'


    );

    $termine = get_posts($args);

    echo "<div class='container' id='termine'>";
    echo "<div class='row'>";
    echo "<div class='col-xl-3 col-lg-3 col-md-3 col-sm-3 col-xs-3'>";
    $jahresplanerPagesArgs = get_pages(
        array(
            'child_of' => '2151',
            'sort_order' => 'ASC',
            'sort_column' => 'menu_order',
            'posts_per_page' => '-1'
        )
    );
    echo '<div class="theo_collection">';
    echo '<a class="planer_theo_link" href="' . get_permalink($jahresplanerPagesArgs[0]->ID) . '" title="Jahresplaner Theo">Jahresplaner Theo</a>';
    echo "<ul>";
    foreach ($jahresplanerPagesArgs as $singlePage) {
        echo '<li><a class="planer_theo_link" href="' . get_permalink($singlePage->ID) . '" title="' . $singlePage->post_title . '">' . $singlePage->post_title . '</a></li>';
    }
    echo "</ul>";
    echo '</div>';
    foreach ($termine as $singleTermin) {
        // List categories --> Link to Category Archive
        $key = get_category(wp_get_post_categories($singleTermin->ID)[0])->name;
        $keyCollection[] = $key;
    }
    $keyCollection = array_unique($keyCollection);
    foreach ($keyCollection as $key) {
        $id_for_link = str_replace(' ', '', $key);
        $id_for_link_new = str_replace('–', '', $id_for_link);
        echo "<div class='termin_menu' data-for='" . str_replace(" ", "", $id_for_link_new) . "' id='" . $id_for_link_new . "'>" . $key . "<br></div>";
    }
    echo '<br><br><img src="/wp-content/themes/keinath/assets/images/fun_learn.png" width="150">';
    echo "</div>";
    echo "<div class='col-xl-9 col-lg-9 col-md-9 col-sm-9 col-xs-9'>";
    echo "<div class='termins'>";
    foreach ($termine as $singleTermin) {
        //echo '<pre>',print_r($singleTermin),'</pre>';
        $key_val = get_category(wp_get_post_categories($singleTermin->ID)[0])->name;
        $key_val = str_replace(' ', '', $key_val);
        $key_val_new = str_replace('–', '', $key_val);


        if (strtotime(explode(" - ", explode(" | ", get_field('start-date', $singleTermin->ID))[0])[0]) < strtotime(date('d.m.Y'))) {
            echo "<div data-for='" . $key_val_new . "' id='" . $key_val_new . "' class='single-termin'>";
            echo "<div class='termintitle'><a href='" . get_permalink($singleTermin->ID) . "'>" . get_field('title', $singleTermin->ID) . "</a></div>";

            echo "<div class='row start-endDate'>
                                            <div class='col-lg-12'>Datum: " . get_field('start-date', $singleTermin->ID) . "</div>
                                    </div>";
            echo "<div class='row start-endDate'>
                                            <div class='col-lg-4'>Ort: " . get_field('location', $singleTermin->ID) . "</div>
                                            <div class='col-lg-4'><span class='link'><a href='" . get_permalink($singleTermin->ID) . "'>Jetzt anmelden</a></span></div>
                                    </div>";
            echo '<br>';
            echo "</div>";
        } else {
            echo "<div data-for='" . $key_val_new . "' id='" . $key_val_new . "' class='single-termin'>";
            echo "<div class='termintitle'><a href='" . get_permalink($singleTermin->ID) . "'>" . get_field('title', $singleTermin->ID) . "</a></div>";

            echo "<div class='row start-endDate'>
                                            <div class='col-lg-12'>Datum: " . get_field('start-date', $singleTermin->ID) . "</div>
                                    </div>";
            echo "<div class='row start-endDate'>
                                            <div class='col-lg-4'>Ort: " . get_field('location', $singleTermin->ID) . "</div>
                                            <div class='col-lg-4'><span class='link'><a href='" . get_permalink($singleTermin->ID) . "'>Jetzt anmelden</a></span></div>
                                    </div>";
            echo '<br>';
            echo "</div>";
        }

        $key_val = get_category(wp_get_post_categories($singleTermin->ID)[0])->name;
        $sortedTermins[$key_val][] = array(
            'title' => get_field('title', $singleTermin->ID),
            'permalink' => get_permalink($singleTermin->ID),
            'start-date' => get_field('start-date', $singleTermin->ID),
            'formatted_strtotime' => strtotime(explode(" - ", explode(" | ", get_field('start-date', $singleTermin->ID))[0])[0]),
            'location' => get_field('location', $singleTermin->ID)
        );
    }
    echo "</div>";

    foreach ($sortedTermins as $key => $value) {
    }
    echo "<pre style='display:none;'>";

    //print_r($sortedTermins);
    echo "</pre>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
}

add_shortcode('termin-collection', 'displayTermine');
function find_index_arr($store_data, $id)
{
    $i = -1;
    foreach ($store_data as &$el) {
        $i++;
        if ($el['id'] == $id) return $i;
    }
    return $i;
}
function mergeCaheData($data_in_cache, $store_data)
{
    $data_in_cache = array_merge($data_in_cache);
    $first_in_cache_id = $data_in_cache[0]['id'];
    $found_key = find_index_arr($store_data, $first_in_cache_id);
    if ($found_key == 0) return $data_in_cache;
    if ($found_key < 0) {
        return array_merge($store_data, $data_in_cache);
    } else {
        $arr_len = count($store_data);
        $to_merge = array_merge(array_slice($store_data, 0, $arr_len - ($arr_len - $found_key)));
        return array_merge($to_merge, $data_in_cache);
    }
}
function getSocialWallPosts($tag, $tmp_file_path, $data_in_cache = null)
{
    // parameter for Instagram API
    $instagram_config = array(
        'access_token' => '1663834427.f1e7d27.8d1598343c5245849cfe96b506097af5',
        'count' => 100
    );
    // get request URL
    $instagram_url  = 'https://api.instagram.com/v1/users/self/media/recent/?';
    $instagram_url .= 'access_token=' . $instagram_config['access_token'] . '&';
    $instagram_url .= 'count=' . $instagram_config['count'];
    $feed_array = [];
    try {
        // get request
        $instagram_json_string = wp_remote_retrieve_body(wp_remote_get($instagram_url));
        // $instagram_json_string = file_get_contents($instagram_url);
        if (!is_wp_error($instagram_json_string)) {
            // if( $instagram_json_string !== false) { //request successfull
            // covert result to json
            $instagram_feed = json_decode($instagram_json_string, true, 512, JSON_BIGINT_AS_STRING);
            $feed_array = $instagram_feed['data'];
            if (count($feed_array) <= 0 || $feed_array === null) return $data_in_cache;
        } else {
            return [];
        }
    } catch (\Throwable $th) {
        return [];
    }
    // filter posts by tag ($tag = bestanden)
    $filtered_feed = array_filter($feed_array, function ($post) use ($tag) {
        return in_array($tag, $post['tags']) || strpos($post['caption']['text'], $tag) !== false;
    });
    // prepare cache data
    $store['data'] = $filtered_feed;
    $store['tag'] = $tag;
    $store['date'] = new DateTime();
    $store_str = json_encode($store);
    if (file_exists($tmp_file_path) && isset($data_in_cache)) {
        // merge the datas if it already in cache exist
        $filtered_feed = mergeCaheData($data_in_cache, $filtered_feed);
        $store['data'] = $filtered_feed;
        unlink($tmp_file_path);
    }
    // write post in cache
    $tmp_file = fopen($tmp_file_path, 'c');
    fwrite($tmp_file, $store_str);
    fclose($tmp_file);
    return $filtered_feed;
}

function link_tags($text)
{
    $re = '/(#[A-Za-z0-9üÜäÄöÖ\-\.\_]+)/m';
    $repl = '<a href="#">$1</a>';
    return preg_replace($re, $repl, $text);
}

function socialWall()
{
    $tmp_file_path = '_instagram_posts_tmp';
    $tag = 'bestanden';
    $max = 6;
    $filtered_feed = [];
    if (file_exists($tmp_file_path)) { // if data exist in cache
        // open cache file
        $handle = fopen($tmp_file_path, 'r');
        $contents = fread($handle, filesize($tmp_file_path));
        fclose($handle);
        $data_in_cache = json_decode($contents, true);
        // if the cahe is empty
        if ($tag !== $data_in_cache['tag']) {
            unlink($tmp_file_path);
            $filtered_feed = getSocialWallPosts($tag, $tmp_file_path);
        } else {
            if (count($data_in_cache['data']) <= 0 || $data_in_cache['data'] === null) {
                unlink($tmp_file_path);
                $filtered_feed = getSocialWallPosts($tag, $tmp_file_path);
            } else {
                // compare date with the current date
                $date = new DateTime($data_in_cache['date']['date']);
                $date_now = new DateTime();
                $date = $date->modify("+1 hour");
                // if is older than one hour
                if ($date > $date_now) {
                    // add new posts to the cahe
                    $filtered_feed = getSocialWallPosts($tag, $tmp_file_path, $data_in_cache['data']);
                } else {
                    $filtered_feed = $data_in_cache['data'];
                }
            }
        }
    } else {
        $filtered_feed = getSocialWallPosts($tag, $tmp_file_path);
    }
    $filtered_feed = array_slice($filtered_feed, 0, 9);
    if (count($filtered_feed) > 0) {
        echo '<section class="social-wall">';
        echo '<div class="container">';
        echo '<div class="row">';
        echo '<div class="social-wall-content">';
        echo '<div class="">';
        echo '<h2 class="" style="color:#000000;">Social Wall</h2>';
        echo '</div>';
        echo '<div class="">';
        echo '<p></p>';
        echo '</div>';

        echo '<div class="row instagram-posts" id="instagram-posts">';
        foreach ($filtered_feed as &$post) {
            echo '<div class="col-12 col-md-6 col-lg-3 insta-post">';
            echo '<div>';
            echo '<div class="post-img">';
            echo '<a href="' . $post['link'] . '" class="instagram-link" target="_blank" rel="noopener noreferrer" style="background-image: url(' . $post['images']['standard_resolution']['url'] . ');">';
            //echo '<img src="' .  . '">';
            echo '</a>';
            echo '</div>';
            echo '<div class="post-text">';
            echo '<p><strong>' . $post['caption']['from']['username'] . ': </strong>' . ($post['caption']['text']) . '</p>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
        if (count($filtered_feed) > $max) {
            //echo '<div class="bottom-blur"> <span></span></div>';
        }
        echo '</div>';
        echo '</div>';

        if (count($filtered_feed) > $max) {
            echo '<div>';
            echo '<div class="border-top" id="more-instagram-post">';
            echo '<div class="lg-down-angle"></span>';
            echo '</div>';
            echo '</div>';
        }


        echo '</div>';
        echo '</div>';
        echo '</section>';
    }
}

add_shortcode('show-socialwall', 'socialWall');


function displaynews()
{


    $return = '<div class="row">';
    $catquery = new WP_Query('cat=19&posts_per_page=-1&order=DESC&orderby=date');
    foreach ($catquery->posts as $singlePost) {
        $return .= '<div class="col-lg-12">';
        $return .= '<h4>' . $singlePost->post_title . '</h4>';
        $return .= $singlePost->post_excerpt;
        $return .= "<br><br>";
        $return .= '<a href="' . get_permalink($singlePost->ID) . '" class="read_more">Mehr lesen »</a>';
        $return .= "<br><br><hr>";
        $return .= "<br>";
        $return .= '</div>';
    }
    $return .= '</div><br>';
    return $return;
}

add_shortcode('news', 'displaynews');


/**
 * Content Hub Post
 */
/**
 *
 * Registration unseres Custom Post Types "Portfolio"
 *
 */
function ah_custom_post_type()
{
    $labels = array(
        'name' => 'Content Hub Einträge',
        'singular_name' => 'Content Hub',
        'menu_name' => 'Content Hub',
        'parent_item_colon' => '',
        'all_items' => 'Alle Einträge',
        'view_item' => 'Eintrag ansehen',
        'add_new_item' => 'Neuer Eintrag',
        'add_new' => 'Hinzufügen',
        'edit_item' => 'Eintrag bearbeiten',
        'update_item' => 'Update Eintrag',
        'search_items' => '',
        'not_found' => '',
        'not_found_in_trash' => '',
    );
    $rewrite = array(
        'slug' => 'Content Hub',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
    );
    $args = array(
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'comments', 'trackbacks',),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => false,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'rewrite' => $rewrite,
        'capability_type' => 'page',
    );
    register_post_type('contenthub', $args);
}
// Hook into the 'init' action
add_action('init', 'ah_custom_post_type', 0);
