<?php
	get_header();
?>
<style>
.row {
	margin-bottom: 40px;
}
.button{
	display: block;
	margin-top: 20px;
	text-align: center;
	padding: 10px;
	max-width: 60%;
}
.button:hover{
	color: #fff;
	text-decoration: none;
}
@media all and (max-width: 769px){
	.container.tmp .row:first-child{
		padding-top: 100px;
	}
}
</style>
	<div class="container tmp">
		<div class="row">
			<div class="col-md-12">
				<h1><?php _e( 'Seite nicht gefunden', 'smpl' ); ?></h1>
				<p>Das tut uns leid! Normalerweise haben wir für alles die passende Lösung. Doch dieses Problem können wir leider gerade nicht beheben! Eventuell hat sich ein Tippfehler eingeschlichen oder die gesuchte Seite wurde verschoben. Aber keine Sorge, Sie haben zentrale Möglichkeiten, trotzdem an Ihr Ziel zu gelangen.</p>
			</div>
		</div>
		<div class="row buttons">
			<div class="col-md-6">
				<h1>Der einfache Weg</h1>
				<p>Beginnen Sie auf unserer Startseite.</p>
				<p><a href="/" class="button">Zur Startseite</a></p>
			</div>
			<div class="col-md-6">
				<h1>Der praktische Weg</h1>
				<p>Profitieren Sie von diesen Seiten.</p>
				<p><a href="/fuehrerschein/" class="button">Führerschein</a></p>
				<p><a href="/kurse-seminare/" class="button">Kurse & Seminare</a></p>
				<p><a href="/termine-service/" class="button">Termine</a></p>
				<p></p>
			</div>
		</div>
	</div>
<?php
	get_footer();
?>