<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package keinath
 * @since keinath 1.0
 */
global $query_string;

wp_parse_str( $query_string, $search_query );
$search = new WP_Query( $search_query );
get_header(); ?>


	<section>

    <section>
            <div class="container">
                <div class="row">
                        <div class="col-lg-12">
                            <div class="mysearch_fieldContainer">
                            <!-- <div class="search-page-form" id="ss-search-page-form"><?php get_search_form(); ?></div> -->
                             <div class="search-page-form">
                                    <form role="search" method="get" class="search-form mysearch_form" action="<?php echo home_url( '/' ); ?>">
                                        
                                        <label>
                                            <div class="row">
                                                <div class="col-lg-12"><span class="screen-reader-text termin_title"><?php echo _x( 'Suche für:', 'label' ) ?></span></div>
                                                <div class="col-lg-10 pr-1"><input type="search" class="search-field mysearch_field form-control"
                                                placeholder="<?php echo esc_attr_x( 'Suchbegriff …', 'placeholder' ) ?>"
                                                value="<?php echo get_search_query() ?>" name="s"
                                                title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" /></div>
                                                <div class="col-lg-2 px-0"><input type="submit" class="search-submit my_search_submit"
                                            value="<?php echo esc_attr_x( 'SUCHE', 'submit button' ) ?>" /></div>
                                            </div>
                                            
                                            
                                        </label>

                                    </form>
                             </div>  
                            </div>   
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="search_result">
                                <?php if ( have_posts() ) : ?>
                                        
                                        <header class="page-header">
                                                <h2 class="search-page-title termin_title"><?php printf( esc_html__( 'Ihr Suchergebnis für: %s', 'keinath' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
                                        </header><!-- .page-header -->
                                       
                                        <!--<header class="page-header">
                                            <h2 class="search-page-title"><?php printf( esc_html__( 'Search Results for: %s', 'keinath' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
                                        </header>--><!-- .page-header -->
                                        
                                        
                                        <div class="row">
                                                <?php /* Start the Loop */ ?>   
                                                <?php while ( have_posts() ) : the_post(); ?>
                                                
                                                    <div class="col-lg-4">
                                                            <div class="single_search">
                                                                <h4 class="search-post-title"><?php the_title(); ?></h4>
                                                                <span class="search-post-excerpt"><?php the_excerpt(); ?></span>
                                                                <span class="search-post-link"><a href="<?php the_permalink(); ?>" class="read_more"><?php //the_permalink(); ?>Mehr lesen »	</a></span>
                                                            </div>
                                                    </div>
                                                
                                                <?php endwhile; ?>
                                        </div>
                                        <?php //the_posts_navigation(); ?>

                                        <?php else : ?>

                                        <?php get_template_part( 'template-parts/content', 'none' ); ?>
                                        <?php //echo '<h2 class="search-page-title">Search result is not found</h2>';?>
                                        <!-- <h2 class="search-page-title"><?php printf( esc_html__( 'Search Results for: %s', 'keinath' ), '<span>' . get_search_query() . '</span>' ); ?> not found</h2> -->
                                        <h2 class="search-page-title termin_title"><?php printf( esc_html__( 'Ihre Suche mit  %s', 'keinath' ), '<span>"' . get_search_query() . '" ergibt leider keine Ergebnisse. Versuchen Sie es mit einem anderen Suchwort.</span>' ); ?> not found</h2>
                                <?php endif; ?>
                        </div>
                        </div>  
                </div>

            </div>
    </section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>