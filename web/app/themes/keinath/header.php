<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	<title><?php wp_title(); ?></title>
	
	<link rel="shortcut icon" href="/favicon.ico" type="image/png" />

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

	<?php wp_head(); ?>
	<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="ac9dd927-806e-40f1-927d-7a3d6451d02b" type="text/javascript" defer></script>
  </head>

  <body>

    <!-- Navigation -->
    <header class="fixed-top">
     
      <div class="container chk">
	  	<div class="mbl_show comment_icon"><i class="fas fa-comment r_icon comment_fa"></i></div> 
        <div class="row">
            <div class="col-lg-4 col-4 chng1">
				<div class="logo scroll_visible"><a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/small-logo.svg" alt="logo" width="74px" class="small"></a></div>
				<div class="logo scroll_hide"><a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg" alt="logo" width="265px" class="big"></a></div>
			</div>
      <div class="col-lg-8 col-8 chng2">
        <button class="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></button>
				<nav class="navbar navbar-expand-lg navbar-dark custom_nav">
			
						<?php wp_nav_menu(array(
							'theme_location'=>'header',
							'menu_class'=>'navbar-nav',
							'menu_id'=>'',
							'container'=>'',
							'container_class'=>'',
							'container_id' =>'',
							'link_after' =>''
						));?>
						<ul class="scroll_visible scroll_Topmenu">
							<li class="mbl"><a href="<?php echo home_url(); ?>/kontakt/" class="top_icon"><i class="fas fa-user r_icon"></i><span class="head_icon">KONTAKT</span></a></li>
							<li class="mbl"><a href="<?php echo home_url(); ?>/suche" class="top_icon" title = "Search Page" >
                <i class="fas fa-search r_icon"></i>
                <span class="head_icon search">SUCHE</span></a>
              </li>
						</ul>
					<!--</div>-->
				</nav>
			</div>
			
        </div>
      </div>
    
	</header>